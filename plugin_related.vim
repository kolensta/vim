set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
set rtp+=~/.fzf
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'tpope/vim-surround'
Plugin 'vim-scripts/AutoComplPop'
Plugin 'tommcdo/vim-exchange'
Plugin 'junegunn/fzf.vim'

Plugin 'preservim/nerdtree'
nnoremap <A-t> :NERDTreeToggle<Cr>

Plugin 'jiangmiao/auto-pairs'
let g:AutoPairsShortcutToggle = '<A-a>'
let g:AutoPairsMoveCharacter = "()"
let g:AutoPairsShortcutJump = ''

Plugin 'easymotion/vim-easymotion'
map <A-w> <Plug>(easymotion-w)
map <A-q> <Plug>(easymotion-b)
map <Leader> <Plug>(easymotion-prefix)

Plugin 'SirVer/ultisnips'
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
