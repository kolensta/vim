" +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
" mapovani znaku
" +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

" navigace misto sipek 4x
imap <A-h> <Esc>ha
imap <A-j> <Esc>ja
imap <A-k> <Esc>ka
imap <A-l> <Esc>la

" nahrade delete
imap <A-m> <Del>

" opusteni insert modu
map <A-i> <Esc>
imap <A-i> <Esc><Right>

" posouvani stranky 4x
imap <A-]> <Esc><C-e>a
imap <A-[> <Esc><C-y>a
nmap <A-]> <C-e>
nmap <A-[> <C-y>

" posouvani znaku 2x
nmap <A-9> xhP
nmap <A-0> xp

"posouvani radku 2x
nmap <A-(> :m-2<CR>
nmap <A-)> :m+1<CR>

" zmena bufferu
nmap <A-}> :bn<CR>
nmap <A-{> :bp<CR>

" pridani prazdneho radku pod aktualni
nmap <C-CR> m':normal o<CR>`'
" pridani prazdneho radku nad aktualni
nmap <S-CR> m':normal O<CR>`'

" psani o radek vys, novy radek
imap <S-CR> <Esc>O

" prochazeni nabizenych slov 2x
imap <A-n> <C-n>
imap <A-p> <C-p>

" scvrkne mezery na jednu, odstrani mezeru u zavorek a pred strednikem
nmap <C-s> :s/\> \+/ /ge \| s/( /(/ge \| s/ )/)/ge \| s/ ;/;/ge<CR>

" komentare pro dokumentaci
imap <A-u> /// \brief 
imap <A-y> /// \param 

" ulozeni, zavolani Make a otevreni vysledku
nmap <A-m> <Esc>:w<CR>:silent make -s<CR>:copen<Cr>
" zavreni okna s vysledky kompilace
nnoremap <A-n> :ccl<CR>

" navigace mezi okny 4x
nmap <A-j> <C-w>j
nmap <A-k> <C-w>k
nmap <A-h> <C-w>h
nmap <A-l> <C-w>l

" seznam bufferu, vice :help :ls
nmap <A-;> :ls<CR>:b  

" ulozeni souboru
nmap <A-s> :w<CR>  

" chlupate zavorky okolo 1 radku
nnoremap <A-f> A {<ESC>jo}<ESC>k^
" odstraneni chlupatych zavorek
nnoremap <A-d> m'f{%dd`'xx^
